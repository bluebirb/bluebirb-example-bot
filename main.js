var Bluebirb = require("bluebirb.js");
var Discord = require("discord.js");

var bot = new Bluebirb.Bot(Discord);

var reaction = require("./commands/reaction.js");

bot.commandHandler.registerCommand(reaction,".reaction","Fun reaction test!");

function messageLog(message) {
  console.log(`${message.author.username} said "${message.content}"`);
}

bot.addListener("message",messageLog);

bot.addListener("userChangeChannel",(user,beforeChannel,afterChannel)=>{
  console.log(`${user.user.username} switched to ${afterChannel.name}`);
});

bot.addListener("userJoinChannel",(user,channel)=>{
  console.log(`${user.user.username} joined ${channel.name}`);
});

bot.addListener("userLeaveChannel",(user,channel)=>{
  console.log(`${user.user.username} left ${channel.name}`);
});

bot.login("token here");
