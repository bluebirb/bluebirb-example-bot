# bluebirb-example-bot

Example bot for the Bluebirb.js Discord bot framework!
The repo for it can be found [here](https://gitlab.com/bluebirb/robin).

# Installation

To install Bluebirb.js, you first need to install Node.js.
You can find that here:
https://nodejs.org/en/download/

Once that's done, you should create a folder that will contain your bot inside of it.
Open a command line there. You can do this (In Windows) by Shift+Right-Clicking inside of the folder,
or you can find the folder's path, and type `cd "path here with quotes"` to get to it.
Next, run `npm install bluebirb.js`. This will install bluebirb.js into that folder,
and then you can import it with a simple `var Bluebirb = require("bluebirb.js")` inside of a Node.js file.

# Reaction Management

Bluebirb makes it very easy to track reactions on a message object, without needing to worry about handling the events yourself.
For example:

```JavaScript
module.exports.callback = function(args,msg,bot) {
  //Args is an array (starting at index 0) of the arguments of the command
  //Msg is the Discord.js message object.
  //bot is the Bluebirb bot object

  msg.channel.send("Pop!").then(m=>{
    m.react("✅");
    //Add the checkmark to the message (ReactionManagers do trigger when the bot client itself makes/removes a reaction)

    var reactionManager = new bot.ReactionManager(m);
    //A ReactionManager is a class that allows for easy reaction management.
    reactionManager.onReact("✅",(reaction,user)=>{
      m.channel.send(user.username+" pressed the ✅!");
      //Sends an addition message!
    });
    
    reactionManager.onRemoveReact("✅",(reaction,user)=>{
      m.channel.send(user.username+" removed the ✅!");
      //Sends a deletion message!
    });
  });
}
```

![](/images/reaction.PNG)

# Events

Currently, Bluebirb only has 3 custom events.
`userJoinChannel, userLeaveChannel, and userChangeChannel`

You can add an event listener to the bot by using ```<Bot Object>.addListener("event name",callback);```

```JavaScript
bot.addListener("userChangeChannel",(user,beforeChannel,afterChannel)=>{
  console.log(`${user.user.username} switched to ${afterChannel.name}`);
});

bot.addListener("userJoinChannel",(user,channel)=>{
  console.log(`${user.user.username} joined ${channel.name}`);
});

bot.addListener("userLeaveChannel",(user,channel)=>{
  console.log(`${user.user.username} left ${channel.name}`);
});
```

![](/images/cmd.PNG)

# Contact
If you have any issues/feedback, please feel free to join the Bluebirb.js Discord server here: https://discord.gg/xPaxaaq
