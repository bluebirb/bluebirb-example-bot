module.exports.callback = function(args,msg,bot) {
  //Args is an array (starting at index 0) of the arguments of the command
  //Msg is the Discord.js message object.
  //bot is the Bluebirb bot object

  msg.channel.send("Pop!").then(m=>{
    m.react("✅");

    var reactionManager = new bot.ReactionManager(m);
    //A ReactionManager is a class that allows for easy reaction management.
    reactionManager.onReact("✅",(reaction,user)=>{
      msg.channel.send(user.username+" pressed the ✅!");
    });
  });
}
